const axios = require('axios');

axios.get("http://senacao.tk/objetos/usuario")
.then(function(res) {
    //console.log(res.data); 
    const user = res.data;
    console.log("--------------Usuário--------------");
    console.log(`Nome: ${user.nome}`);
    console.log(`Email: ${user.email}`);
    console.log(`Telefone: ${user.telefone}`);
    console.log("-----------Conhecimentos-----------");
    user.conhecimentos.forEach(function(conhecimento, index){
        console.log(`${index+1} ${conhecimento}`)
    })
    console.log("--------------Endereço-------------");
    console.log(`Rua: ${user.endereco.rua}`);
    console.log(`Número: ${user.endereco.numero}`);
    console.log(`Bairro: ${user.endereco.bairro}`);
    console.log(`Cidade: ${user.endereco.cidade}`);
    console.log(`UF: ${user.endereco.uf}`);
    console.log("------------Qualificações----------");
    user.qualificacoes.forEach(function(qualificacao){
        console.log(`Nome: ${qualificacao.nome}`);
        console.log(`Instituição: ${qualificacao.instituicao}`);
        console.log(`Ano: ${qualificacao.ano}\n`);
    });
});